import React from "react"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import { AppCard, AppCardContent, AppCardTitle, AppCardActions } from "./AppCard"
import ArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft"
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRight"
import AppSection from "./AppSection"
import CardImage from "../images/undraw_code_review_l1q9.svg"

const useStyles = makeStyles((theme) => {
	return {
		root: {
			flexGrow: 1,
			padding: theme.spacing(2),
			background: theme.background
		},
		main: {
			flexBasis: "100%",
			borderRadius: "10px",
			boxShadow: "0 0 3px 0 #ccc",
			background: "#fff",
			padding: theme.spacing(2)
		},
		cardButton: {
			borderRadius: "100px",
			margin: theme.spacing(2)
		}
	}
})

function Main(props) {
	const classes = useStyles()
	return (
		<Box className={classes.root}>
			<main className={classes.main}>
				{props.data.links.map((link, index) => (
					<AppSection key={index} title={link.primary} id={link.id}>
						{link.data && Array.isArray(link.data) ? (
							<Grid container spacing={2}>
								{link.data.map((data, index) => {
									if (data instanceof Object) {
										return (
											<Grid item key={index}>
												<AppCard src={CardImage}>
													<AppCardContent>
														<AppCardTitle title={data.name} />
													</AppCardContent>
													<AppCardActions>
														<Button
															color="secondary"
															classes={{ root: classes.cardButton }}
														>
															Project Site
															<ArrowRightIcon />
														</Button>
														<Button
															color="secondary"
															classes={{ root: classes.cardButton }}
														>
															<ArrowLeftIcon />
															Source Code
															<ArrowRightIcon />
														</Button>
													</AppCardActions>
												</AppCard>
											</Grid>
										)
									}
									return (
										<Grid container item direction="column" key={index}>
											<Typography variant="h4" component="h4" color="primary" key={index}>
												<ArrowLeftIcon />
												{data}
												<ArrowRightIcon />
											</Typography>
										</Grid>
									)
								})}
							</Grid>
						) : (
							<Typography variant="body1" component="p">
								{link.data}
							</Typography>
						)}
					</AppSection>
				))}
			</main>
		</Box>
	)
}

export default Main
