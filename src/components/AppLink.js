import React from "react"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
	link: {
		color: "inherit",
		textDecoration: "none"
	}
})

export function AppLink({ to, children, rest }) {
	const classes = useStyles()
	return (
		<Link className={classes.link} to={to} {...rest}>
			{children}
		</Link>
	)
}

export const AppLinkRef = React.forwardRef((props, ref) => (
	<div ref={ref}>
		<Link {...props} />
	</div>
))

AppLink.propTypes = {
	to: PropTypes.string.isRequired,
	children: PropTypes.node,
	rest: PropTypes.object
}
