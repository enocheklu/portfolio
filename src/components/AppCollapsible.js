import React from "react"
import PropTypes from "prop-types"
import ExpansionPanel from "@material-ui/core/ExpansionPanel"
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary"
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import Typography from "@material-ui/core/Typography"
import Avatar from "@material-ui/core/Avatar"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import { makeStyles } from "@material-ui/core/styles"
import WorkIcon from "@material-ui/icons/Work"

const useStyles = makeStyles(theme => ({
	root: {
		width: "100%"
	},
	appCollapsible: {
		background: "none",
		boxShadow: "none"
	},
	appCollapsibleHeadingBox: {
		display: "flex",
		flexDirection: "column",
		marginLeft: theme.spacing(2)
	},
	appCollapsibleHeading: {
		fontWeight: theme.typography.fontWeightBold,
		fontSize: theme.typography.pxToRem(16)
	},
	appCollapsibleSubHeading: {
		fontWeight: theme.typography.fontWeightLight,
		fontSize: theme.typography.pxToRem(12)
	}
}))

export default function AppCollapsible({ title, subtitle, children }) {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<ExpansionPanel className={classes.appCollapsible}>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<Avatar>
						<WorkIcon />
					</Avatar>
					<div className={classes.appCollapsibleHeadingBox}>
						<Typography variant="button" className={classes.appCollapsibleHeading}>
							{title}
						</Typography>
						<Typography className={classes.appCollapsibleSubHeading}>{subtitle}</Typography>
					</div>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<Typography>{children}</Typography>
				</ExpansionPanelDetails>
			</ExpansionPanel>
		</div>
	)
}

AppCollapsible.propTypes = {
	title: PropTypes.string.isRequired,
	subtitle: PropTypes.string,
	children: PropTypes.node.isRequired
}
