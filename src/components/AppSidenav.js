import React from "react"
import PropTypes from "prop-types"
import CssBaseline from "@material-ui/core/CssBaseline"
import Container from "@material-ui/core/Container"
import Drawer from "@material-ui/core/Drawer"
import Hidden from "@material-ui/core/Hidden"
import { makeStyles } from "@material-ui/core/styles"
import Avatar from "@material-ui/core/Avatar"
import Link from "@material-ui/core/Link"
import Typography from "@material-ui/core/Typography"
import PersonIcon from "@material-ui/icons/Person"
import AvatarImage2 from "../images/woman-avatar-1.jpg"
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"

const drawerWidthLargeScreen = "30%"
const drawerWidthSmallScreen = "80%"

const useStyles = makeStyles(theme => ({
	drawer: {
		[theme.breakpoints.down("xs")]: {
			width: drawerWidthSmallScreen
		},
		[theme.breakpoints.up("sm")]: {
			width: drawerWidthLargeScreen
		},
		flexShrink: 0
	},
	drawerPaper: {
		[theme.breakpoints.only("xs")]: {
			width: drawerWidthSmallScreen
		},
		[theme.breakpoints.up("sm")]: {
			width: drawerWidthLargeScreen,
			padding: theme.spacing(1)
		},
		border: "none",
		boxShadow: "0 0 5px 1px rgba(100, 100, 100, 0.2)"
	},
	profileName: {
		fontSize: "1.5rem",
		fontWeight: 400
	},
	drawerTitle: {
		fontSize: "1.5rem"
	},
	drawerSubTitle: {
		color: "rgba(20, 20, 20, 0.7)"
	},
	profileUserName: {
		fontWeight: 300,
		color: "rgba(20, 20, 20, 0.7)"
	},
	avatarInfoBox: {
		[theme.breakpoints.up("sm")]: {
			paddingTop: theme.spacing(2),
			paddingBottom: theme.spacing(3)
		}
	},
	avatar: {
		width: 150,
		height: 150,
		marginBottom: theme.spacing(2)
	},
	avatarBox: {
		padding: theme.spacing(4),
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center"
	}
}))

function AppSidenav({ avatar, profile }) {
	const classes = useStyles()

	const AppDrawer = (
		<div>
			<Container>
				<div className={classes.avatarBox}>
					<Avatar alt="Avatar" src={AvatarImage2} className={classes.avatar}>
						{avatar}
					</Avatar>
					<Typography className={classes.profileName} gutterBottom variant="h2" component="h2">
						{profile.name}
					</Typography>
					<Typography className={classes.drawerTitle} gutterBottom variant="h1" component="h1">
						{profile.data.jobTitles}
					</Typography>
					<br />
					<br />
					<Typography variant="body1" className={classes.drawerSubTitle} component="p">
						{profile.data.status}
					</Typography>
					<br />
					<br />
					<Grid container alignItems="center" spacing={3}>
						{profile.data.social.map((obj, index) => (
							<Grid item zeroMinWidth key={index}>
								<Link href={obj.link} target="_blank" rel="noopener">
									<Avatar>
										<PersonIcon />
									</Avatar>
								</Link>
							</Grid>
						))}
					</Grid>
				</div>
			</Container>
		</div>
	)
	return (
		<nav className={classes.drawer}>
			<CssBaseline />
			<Hidden xsDown implementation="css">
				<Drawer
					classes={{
						paper: classes.drawerPaper
					}}
					anchor="left"
					variant="permanent"
					open
				>
					{AppDrawer}
				</Drawer>
			</Hidden>
		</nav>
	)
}

AppSidenav.propTypes = {
	avatar: PropTypes.string,
	data: PropTypes.object
}

export default AppSidenav
