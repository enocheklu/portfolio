import React from "react"
import Button from "@material-ui/core/Button"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import TextField from "@material-ui/core/TextField"
import Paper from "@material-ui/core/Paper"
import InputAdornment from "@material-ui/core/InputAdornment"
import IconButton from "@material-ui/core/IconButton"
import { makeStyles } from "@material-ui/core/styles"
import SearchIcon from "@material-ui/icons/Search"
import EmailIcon from "@material-ui/icons/Email"
import PersonIcon from "@material-ui/icons/Person"
import Visibility from "@material-ui/icons/Visibility"
import VisibilityOff from "@material-ui/icons/VisibilityOff"
import { InputBase } from "@material-ui/core"

const useStyles = makeStyles(theme => ({
	appFormSearchBox: {
		padding: theme.spacing(2)
	},
	appFormInput: {
		margin: theme.spacing(2),
		width: "100%"
	},
	appFormButton: {
		margin: theme.spacing(2)
	}
}))

function AppForm({ type }) {
	switch (type) {
		case "contact":
			return <AppContactForm />
			
		case "register":
			return <AppRegisterForm />
			
		case "login":
			return <AppLoginForm />
			
		case "search":
			return <AppSearchForm />
			
		default:
			return <AppContactForm />
	}
}

function AppContactForm() {
	return (
		<form>
			<Input
				placeholder="First name"
				startAdornment={
					<InputAdornment position="start">
						<PersonIcon />
					</InputAdornment>
				}
			/>
			<Input placeholder="Last name" />
		</form>
	)
}

function AppRegisterForm() {
	const [formValues, setFormValues] = React.useState({
		showPassword: false
	})

	const handleClickShowPassword = () => {
		setFormValues({ ...formValues, showPassword: !formValues.showPassword })
	}

	const handleMouseDownPassword = event => {
		event.preventDefault()
	}

	const classes = useStyles()

	return (
		<form>
			<FormControl fullWidth required>
				<TextField
					placeholder="First name"
					InputProps={{
						startAdornment: (
							<InputAdornment position="start">
								<PersonIcon color="secondary" />
							</InputAdornment>
						)
					}}
					label="First name"
					margin="normal"
					variant="outlined"
					className={classes.appFormInput}
				/>
				<TextField
					placeholder="Last name"
					InputProps={{
						startAdornment: (
							<InputAdornment position="start">
								<PersonIcon color="secondary" />
							</InputAdornment>
						)
					}}
					label="Last name"
					margin="normal"
					variant="outlined"
					className={classes.appFormInput}
				/>
			</FormControl>
			<br />
			<FormControl fullWidth required>
				<TextField
					placeholder="Email"
					InputProps={{
						startAdornment: (
							<InputAdornment position="start">
								<EmailIcon color="secondary" />
							</InputAdornment>
						)
					}}
					label="Email"
					margin="normal"
					variant="outlined"
					className={classes.appFormInput}
				/>
			</FormControl>
			<br />
			<TextField
				placeholder="Username"
				InputProps={{
					startAdornment: (
						<InputAdornment position="start">
							<PersonIcon color="secondary" />
						</InputAdornment>
					)
				}}
				label="Username"
				margin="normal"
				variant="outlined"
				className={classes.appFormInput}
			/>
			<TextField
				placeholder="Password"
				type={formValues.showPassword ? "text" : "password"}
				InputProps={{
					endAdornment: (
						<InputAdornment position="end">
							<IconButton
								edge="end"
								aria-label="toggle password visibility"
								onClick={handleClickShowPassword}
								onMouseDown={handleMouseDownPassword}
							>
								{formValues.showPassword ? <Visibility color="secondary" /> : <VisibilityOff />}
							</IconButton>
						</InputAdornment>
					)
				}}
				label="Password"
				margin="normal"
				variant="outlined"
				className={classes.appFormInput}
			/>
			<br />
			<Button variant="contained" color="primary" className={classes.appFormButton}>
				Register
			</Button>
		</form>
	)
}
function AppLoginForm() {
	const [formValues, setFormValues] = React.useState({
		showPassword: false
	})

	const handleClickShowPassword = () => {
		setFormValues({ ...formValues, showPassword: !formValues.showPassword })
	}

	const handleMouseDownPassword = event => {
		event.preventDefault()
	}

	const classes = useStyles()

	return (
		<form>
			<FormControl required>
				<TextField
					placeholder="Username"
					InputProps={{
						startAdornment: (
							<InputAdornment position="start">
								<PersonIcon color="secondary" />
							</InputAdornment>
						)
					}}
					label="Username"
					margin="normal"
					variant="outlined"
					className={classes.appFormInput}
				/>
			</FormControl>
			<FormControl required>
				<TextField
					placeholder="Password"
					type={formValues.showPassword ? "text" : "password"}
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconButton
									edge="end"
									aria-label="toggle password visibility"
									onClick={handleClickShowPassword}
									onMouseDown={handleMouseDownPassword}
								>
									{formValues.showPassword ? <Visibility color="secondary" /> : <VisibilityOff />}
								</IconButton>
							</InputAdornment>
						)
					}}
					label="Password"
					margin="normal"
					variant="outlined"
					className={classes.appFormInput}
				/>
			</FormControl>
			<Button variant="contained" color="primary" className={classes.appFormButton}>
				Login
			</Button>
		</form>
	)
}

function AppSearchForm() {
	const classes = useStyles()
	return (
		<div className={classes.appFormSearchBox}>
			<TextField InputProps={{ startAdornment: <SearchIcon color="disabled" /> }} placeholder="Search Here" />
		</div>
	)
}

export default AppForm
