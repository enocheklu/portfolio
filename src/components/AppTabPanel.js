import React from "react"
import PropTypes from "prop-types"
import Box from "@material-ui/core/Box"

function AppTabPanel(props) {
	const { children, index, value } = props
	return (
		<Box
			component="div"
			role="apptabpanel"
			hidden={value !== index}
			id={`app-tabpanel-${index}`}
			aria-labelledby={`app-tab-${index}`}
		>
			<Box p={3}>{children}</Box>
		</Box>
	)
}

AppTabPanel.propTypes = {
	children: PropTypes.node || PropTypes.string,
	index: PropTypes.number.isRequired,
	value: PropTypes.number.isRequired
}

export default AppTabPanel
