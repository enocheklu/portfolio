import React from "react"
import PropTypes from "prop-types"
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import Container from "@material-ui/core/Container"
import { makeStyles } from "@material-ui/styles"

const useStyles = makeStyles(theme => ({
	root: props => ({
		[theme.breakpoints.down("xs")]: {
			padding: theme.spacing(2)
		},
		display: "flex",
		padding: `calc(10px * ${props.padding})`,
		height: "100%",
		flexDirection: props.flow,
		justifyContent: "center",
		textAlign: "center",
		background: props.background
	}),
	content: {
		color: "rgba(20, 20, 20, 0.5)"
	}
}))

function AppSection({ title, subtitle, padding, flow, titleVariant, background, center, children }) {
	const classes = useStyles({ center, padding, flow, center, background })
	return (
		<Box component="section" classes={{ root: classes.root }}>
			<Container maxWidth="md">
				<Typography
					variant="h2"
					color="textPrimary"
					style={{
						fontSize: titleVariant === "small" ? "1rem" : titleVariant === "medium" ? "1.5rem" : ""
					}}
				>
					{title}
				</Typography>
				{subtitle && (
					<Typography variant="subtitle1" color="textSecondary">
						{subtitle}
					</Typography>
				)}
				<br />
				<Typography variant="body1" component="div" className={classes.content}>
					{children}
				</Typography>
			</Container>
		</Box>
	)
}

AppSection.propTypes = {
	title: PropTypes.string,
	subtitle: PropTypes.string,
	noPadding: PropTypes.bool,
	titleVariant: PropTypes.string,
	padding: PropTypes.number,
	card: PropTypes.bool,
	colors: PropTypes.object,
	children: PropTypes.any
}

export default AppSection
