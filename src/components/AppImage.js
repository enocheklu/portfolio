import React from "react"
import PropTypes from "prop-types"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
	appImageContainer: {
		position: "relative"
	},
	appImage: {
		width: "100%",
		height: "auto"
	},
	appBackgroundContainer: {
		zIndex: 1,
		display: "flex",
		alignItems: "center",
		justifyContent: "center"
	},
	appImageChildren: {
		position: "absolute",
		top: "5%",
		zIndex: 2
	}
})

function AppImage({ src, alt, background, children }) {
	const classes = useStyles()
	return background ? (
		<div className={classes.appImageContainer}>
			<div className={classes.appBackgroundContainer}>
				<img src={src} alt={alt} className={classes.appBackgroundImage} />
			</div>
			<div className={classes.appImageChildren}>{children}</div>
		</div>
	) : (
		<img src={src} alt={alt} className={classes.appImage} />
	)
}

AppImage.propTypes = {
	background: PropTypes.bool,
	src: PropTypes.string.isRequired,
	alt: PropTypes.string.isRequired,
	children: PropTypes.node
}

export default AppImage
