import React from "react"
import PropTypes from "prop-types"
import Typography from "@material-ui/core/Typography"
import Card from "@material-ui/core/Card"
import CardMedia from "@material-ui/core/CardMedia"
import CardContent from "@material-ui/core/CardContent"
import CardActions from "@material-ui/core/CardActions"
import { makeStyles } from "@material-ui/styles"

const useStyles = makeStyles(theme => ({
	appCard: {
		margin: theme.spacing(2)
	},
	appCardMedia: {
		height: 200
	},
	appCardContent: {
		paddingLeft: theme.spacing(2)
	}
}))

export function AppCard({ avatar, children }) {
	return (
		<Card className={useStyles().appCard}>
			<CardMedia image={avatar} className={useStyles().appCardMedia} />
			{children}
		</Card>
	)
}

export function AppCardText({ children }) {
	return (
		<Typography variant="body2" color="textSecondary" component="p">
			{children}
		</Typography>
	)
}

export function AppCardTitle({ smallText, mediumText, largeText, children }) {
	return (
		<Typography
			gutterBottom
			variant={smallText ? "h6" : mediumText ? "h4" : largeText ? "h2" : "body1"}
			component="h2"
		>
			{children}
		</Typography>
	)
}

export function AppCardContent({ children }) {
	return <CardContent className={useStyles().appCardContent}>{children}</CardContent>
}

export function AppCardActions(props) {
	return <CardActions disableSpacing>{props.children}</CardActions>
}

AppCard.propTypes = {
	avatar: PropTypes.string,
	children: PropTypes.any
}

AppCardText.propTypes = {
	children: PropTypes.string || PropTypes.node
}

AppCardContent.propTypes = {
	children: PropTypes.node || PropTypes.string
}

AppCardActions.propTypes = {
	children: PropTypes.node.isRequired
}
