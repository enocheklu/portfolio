import React from "react"
import PropTypes from "prop-types"
import Container from "@material-ui/core/Container"
import Avatar from "@material-ui/core/Avatar"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import WorkIcon from "@material-ui/icons/Work"

const useStyles = makeStyles(theme => ({
	appTimeLineBox: {
		borderLeft: `2px solid ${theme.palette.secondary.light}`,
		paddingLeft: theme.spacing(5),
		paddingTop: theme.spacing(3),
		paddingBottom: theme.spacing(3)
	},
	appTimeLineIndicator: {
		background: theme.palette.primary.main,
		marginLeft: "-20px"
	}
}))

function AppTimeLine({ timeLines, horizontal }) {
	const classes = useStyles()
	return (
		<Container>
			{timeLines.map((timeLine, index) => (
				<React.Fragment key={index}>
					<div style={{ display: "flex", alignItems: "center" }}>
						<Avatar className={classes.appTimeLineIndicator}>
							<WorkIcon />
						</Avatar>
						<div style={{ display: "flex", flexDirection: "column" }}>
							<Typography
								variant="h2"
								color="textPrimary"
								style={{
									fontSize: "1.2rem",
									fontWeight: 400,
									paddingLeft: "40px"
								}}
							>
								{timeLine.name}
							</Typography>
							<Typography
								variant="subtitle2"
								color="textSecondary"
								style={{
									paddingLeft: "40px"
								}}
							>
								{timeLine.subtitle}
							</Typography>
						</div>
					</div>
					<Container className={classes.appTimeLineBox}>
						<Typography variant="body2" style={{ fontWeight: 300 }} component="div">
							{timeLine.content}
						</Typography>
					</Container>
				</React.Fragment>
			))}
		</Container>
	)
}

AppTimeLine.propTypes = {
	timeLines: PropTypes.array
}

export default AppTimeLine
