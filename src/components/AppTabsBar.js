import React from "react"
import PropTypes from "prop-types"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import CircleIcon from "@material-ui/icons/PanoramaFishEye"
import { withStyles } from "@material-ui/core/styles"

function tabProps(index) {
	return {
		id: `app-tab-${index}`,
		"aria-controls": `app-tabpanel-${index}`
	}
}

const CustomTabsBar = withStyles({
	indicator: {
        width: "5px",
        borderRadius: "10px"
	}
})(Tabs)

function AppTabsBar(props) {
	const { tabs, ...rest } = props
	return (
		<CustomTabsBar orientation="vertical" {...rest}>
			{tabs && tabs.map((label, index) => <Tab label={label} key={index} {...tabProps(index)} />)}
		</CustomTabsBar>
	)
}

AppTabsBar.propTypes = {
	tabs: PropTypes.array.isRequired
}

export default AppTabsBar
