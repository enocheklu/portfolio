import React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { MuiThemeProvider } from "@material-ui/core/styles"
import HomePage from "./pages/HomePage"
import LoginPage from "./pages/LoginPage"
import AddPortfolioPage from "./pages/AddPortfolioPage"
import EditPortfolioPage from "./pages/EditPortfolioPage"
import PortfoliosPage from "./pages/PortfoliosPage"
import Portfolio from "./pages/Portfolio"
import appTheme from "./themes/appTheme/appTheme"
import portfolios from "./data/portfolios"

function App(props) {
	return (
		<Router>
			<MuiThemeProvider theme={appTheme}>
				<Switch>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/:id/dashboard" component={EditPortfolioPage} />
					<Route exact path="/:id" render={() => <Portfolio data={portfolios[0]} />} />
					<Route exact path="/login" component={LoginPage} />
					<Route exact path="/portfolios" component={PortfoliosPage} />
					<Route exact path="/portfolios/new" component={AddPortfolioPage} />
					<Route path="*" render={() => "404 NOT FOUND"} />
				</Switch>
			</MuiThemeProvider>
		</Router>
	)
}

export default App
