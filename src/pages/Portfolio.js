import React from "react"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import CssBaseline from "@material-ui/core/CssBaseline"
import Typography from "@material-ui/core/Typography"
import AppTimeLine from "../components/AppTimeLine"
import { makeStyles } from "@material-ui/core/styles"
import { AppCard, AppCardContent, AppCardTitle, AppCardActions } from "../components/AppCard"
import ArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft"
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRight"
import AppSection from "../components/AppSection"
import CardImage from "../images/undraw_code_review_l1q9.svg"
import ReactMarkDown from "react-markdown"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import ListItemText from "@material-ui/core/ListItemtext"
import Avatar from "@material-ui/core/Avatar"

const useStyles = makeStyles(theme => {
	return {
		root: {
			display: "flex"
		},
		content: {
			flexGrow: 1
		},
		appBar: {
			top: theme.spacing(2),
			marginLeft: "400px",
			borderRadius: "10px"
		}
	}
})

const about = `
# Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. 
Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.

## Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
Proin pharetra nonummy pede. Mauris et orci.
** Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.
Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. 
Mauris eget neque at sem venenatis eleifend. Ut nonummy.
Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla.
Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat. **
`

const workTimeLineData = [
	{
		name: "Fries and Co.",
		subtitle: "2017 - present | UI/UX Developer"
	},
	{
		name: "Berch Industries",
		subtitle: "2012 - 2015 | Backend Developer"
	},
	{
		name: "Tech and Co.",
		subtitle: "2010 - 2012 | HR Manager"
	},
	{
		name: "Kirk's Star Hotel",
		subtitle: "2005 - 2009 | Senior Software Engineer"
	}
]

function PortfolioPage({ data }) {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<CssBaseline />
			<main className={classes.content}>
				<AppSection title="About" padding={4} titleVariant="medium">
					<ReactMarkDown source={about} escapeHtml={false} />
				</AppSection>
				<AppSection title="Work Experience" padding={4} titleVariant="medium">
					<br />
					<br />
					<AppTimeLine timeLines={workTimeLineData} />
				</AppSection>
				<AppSection title="Skills" padding={4} titleVariant="medium">
					<AppTimeLine timeLines={data.skills} />
					{/* {data.skills.map((skill, index) => (
							{skill.certifications !== "none" && (
								<AppSection title="Certificates" key={index} mediumText noPadding>
									{skill.certifications.map((cert, index) => (
										<React.Fragment key={index}>
											<List>
												<ListItem button component="a" href={cert.url}>
													<ListItemAvatar>
														<Avatar>{cert.issuer[0]}</Avatar>
													</ListItemAvatar>
													<ListItemText variant="body2">
														<Typography variant="button">{cert.issuer}</Typography>
														<Typography variant="body2">{cert.url}</Typography>
													</ListItemText>
												</ListItem>
											</List>
										</React.Fragment>
									))}
								</AppSection>
							)}
						</AppTimeLine>
					))} */}
				</AppSection>
				<AppSection title="Projects">
					<Grid container spacing={2}>
						{data.projects.map((project, index) => (
							<Grid xs={12} md={6} item key={index}>
								<AppCard avatar={CardImage}>
									<AppCardContent>
										<AppCardTitle title={project.name} />
									</AppCardContent>
								</AppCard>
							</Grid>
						))}
					</Grid>
				</AppSection>
			</main>
		</div>
	)
}

export default PortfolioPage
