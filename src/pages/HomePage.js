import React from "react"
import CssBaseline from "@material-ui/core/CssBaseline"
import Grid from "@material-ui/core/Grid"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import AppSection from "../components/AppSection"
import { AppLink, AppLinkRef } from "../components/AppLink"
import WorkIcon from "@material-ui/icons/Work"
import { makeStyles } from "@material-ui/core/styles"
import AppImage from "../components/AppImage"
import Avatar from "@material-ui/core/Avatar"
import FallImage from "../images/undraw_fall_thyk.svg"
import FeaturesImage from "../images/undraw_features_overview_jg7a.svg"
import SomeImage from "../images/undraw_code_typing_7jnv.svg"
import AppForm from "../components/AppForm"

const appBarLinks = [
	{ title: "Login", url: "/login" },
	{ title: "Contact", url: "/contact" },
	{ title: "About", url: "/about" }
]

const workTimeLineData = [
	{
		name: "Fries and Co.",
		subtitle: "2017 - present | UI/UX Developer"
	},
	{
		name: "Berch Industries",
		subtitle: "2012 - 2015 | Backend Developer"
	},
	{
		name: "Tech and Co.",
		subtitle: "2010 - 2012 | HR Manager"
	},
	{
		name: "Kirk's Star Hotel",
		subtitle: "2005 - 2009 | Senior Software Engineer"
	}
]

const useStyles = makeStyles(theme => ({
	root: {
		display: "flex"
	},
	content: {
		flexGrow: 1
	},
	appBarTitle: {
		flexGrow: 1
	},
	appBarLink: {
		margin: theme.spacing(1)
	}
}))

function HomePage() {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar>
				<Toolbar>
					<Typography color="primary" variant="h5" component="h1" className={classes.appBarTitle}>
						<AppLink to="/">Portfolio.io</AppLink>
					</Typography>
					{appBarLinks.map((link, index) => (
						<Button
							key={index}
							color="secondary"
							variant="text"
							component={AppLinkRef}
							to={link.url}
							className={classes.appBarLink}
						>
							{link.title}
						</Button>
					))}
				</Toolbar>
			</AppBar>
			<main className={classes.content}>
				<Toolbar />
				<AppSection title="Simple yet powerful" flow="column">
					<Typography variant="body1" component="p" style={{ fontSize: "1.3rem" }}>
						A platform for everyone
					</Typography>
					<br />
					<br />
					<Button variant="contained" color="secondary" style={{ padding: "10px 20px", fontSize: "1rem" }}>
						Try for free now
					</Button>
				</AppSection>
				<AppSection title="Lorem ipsum dolor..." flow="column" background="#eee">
					<Typography variant="body1" component="p" style={{ fontSize: "1.3rem" }}>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce
						posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, viverra imperdiet enim.
						Fusce est. Vivamus a tellus.
					</Typography>
					<br />
					<Button variant="contained" color="secondary" style={{ padding: "10px 20px", fontSize: "1rem" }}>
						Try for free now
					</Button>
				</AppSection>
			</main>
		</div>
	)
}

export default HomePage
