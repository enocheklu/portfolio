import React from "react"
import Button from "@material-ui/core/Button"
import Container from "@material-ui/core/Container"
import Typography from "@material-ui/core/Typography"
import AppForm from "../components/AppForm"
import AppSection from "../components/AppSection"
import { AppLinkRef } from "../components/AppLink"
import AppImage from "../components/AppImage"
import LoginPageBackDrop from "../images/undraw_personal_information_962o.svg"

function AppLoginPage(props) {
	return (
		<Container>
			<AppSection title="Login" flow="column" padding={4}>
				<AppForm type="login" />
				<AppSection>
					<Typography variant="body2">Don't have an account?</Typography>
					<Button color="secondary" component={AppLinkRef} to="/register">
						Register
					</Button>
				</AppSection>
			</AppSection>
            <AppImage src={LoginPageBackDrop} alt="" />
		</Container>
	)
}

export default AppLoginPage
