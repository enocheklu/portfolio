import { createMuiTheme } from "@material-ui/core/styles"
import amber from "@material-ui/core/colors/amber"
import blue from "@material-ui/core/colors/blue"

export let AppTheme = createMuiTheme({
	palette: {
		primary: {
			contrastText: "#fff",
			main: amber[600],
			dark: amber[700]
		},
		secondary: {
			contrastText: "#fff",
			main: blue[600],
			dark: blue[700]
		},
		background: {
			default: "#fafafa"
		},
		text: {
			light: "#fff",
			primary: "#335",
			secondary: "rgba(0, 0, 0, 0.5)"
		}
	}
})
