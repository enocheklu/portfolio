import { createMuiTheme } from "@material-ui/core/styles"
import purple from "@material-ui/core/colors/purple"
import amber from "@material-ui/core/colors/amber"

export default createMuiTheme({
	palette: {
		primary: {
			contrastText: "#fff",
			main: purple[500],
			dark: purple[700]
		},
		secondary: {
			contrastText: "#fff",
			main: amber[500],
			dark: amber[700]
		},
		background: {
			default: "#fff"
		},
		text: {
			light: "rgba(20, 20, 20, 0.5)",
			primary: "#333",
			secondary: "rgba(10, 10, 10, 0.5)"
		}
	}
})
